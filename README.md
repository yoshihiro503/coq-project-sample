The Sample Project using Coq

## gitlab CI

[![pipeline status](https://gitlab.com/yoshihiro503/coq-project-sample/badges/master/pipeline.svg)](https://gitlab.com/yoshihiro503/coq-project-sample/commits/master)


https://gitlab.com/yoshihiro503/coq-project-sample/pipelines

## Coqdoc documents

https://yoshihiro503.gitlab.io/coq-project-sample/Main.html

## For developers

### Requirement

- Coq

### How to build

```bash
./configure.sh
make
```
